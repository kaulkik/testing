#!/usr/bin/env bash
LATEST_APK=$(ls -lrt ./app/build/outputs/apk/debug/*.apk | tail -1 | awk -F" " '{ print $9 }') #Pick the latest build apk.
BUILD_DATE=`date +%Y-%m-%d` #optional -- For changelog title.
FILE_NAME=$(basename $BUILD_DATE+$LATEST_APK .apk)".apk"
FILE_TITLE=$(basename $LATEST_APK .apk) #optional -- For changelog title..
